import json
import os
import re
import sys
import urllib.parse

from bs4 import BeautifulSoup, SoupStrainer
import requests

data_dir = "./data"


def windows_encode(string:str):
    if string.upper() in ["CON", "PRN", "AUX", "NUL"] or re.match(r"COM[1-9]|LPT[1-9]", string):
        return string + "_"
    return string.replace("<", "&lt;").replace(">", "&gt;").replace(":", "&colon;").replace("\"", "&quot;")\
        .replace("/", "&sol;").replace("\\", "&bsol;").replace("|", "&vert;").replace("?", "&quest;")\
        .replace("*", "&ast")


def get_page():
    while True:
        url = input("Please input full URL: ") if len(sys.argv) < 2 else sys.argv[1]
        if url.find("wikipedia.org") == -1:
            print("Non-Wikipedia URL specified. Try again.")
            continue
        if url is not None:
            break
    anchor_index = url.rfind('#') if url.rfind("%23") == -1 else url.rfind("%23")
    if anchor_index == -1:
        parsed_url = f"{url[:url.rfind('/')+1]}{urllib.parse.quote(urllib.parse.unquote(url[url.rfind('/')+1:]))}"
    else:
        url = url[:anchor_index]
        parsed_url = f"{url[:url.rfind('/')+1]}{urllib.parse.quote(urllib.parse.unquote(url[url.rfind('/')+1:anchor_index]))}"
    page = requests.get(parsed_url)
    if page.status_code != 200:
        print(f"Page could not be accessed. Status code: {page.status_code}")
        exit(-1)
    return page, url


def remove_unneeded_elements(soup):
    removal_list = soup.find_all(id="toc") + soup.find_all(class_=re.compile(".*references.*")) + soup.find_all(class_="printfooter") \
                   + soup.find_all(class_="mw-editsection") + soup.find_all("sup") + soup.find_all(class_="NavFrame") + soup.find_all(class_="external text") \
                   + soup.find_all(class_="extiw") + soup.find_all(class_=re.compile(".*magiclink-isbn.*")) + soup.find_all("a", class_="image") \
                   + soup.find_all(class_="internal") + soup.find_all("a", class_="new") + soup.find_all(class_="navbox") \
                   + soup.find_all(role="navigation")
    for element in removal_list:
        element.decompose()


def extract_links(soup, url):
    links = soup.find_all("a")
    links = list(dict.fromkeys(links))
    link_dict = dict(name=urllib.parse.unquote(url[url.rfind("/")+1:].replace("_", " ")), links=list())
    for element in links:
        # decoded_link = urllib.parse.unquote(element.get("href"))
        if element.get("href")[0] not in "#hf":
            link_dict["links"].append(url[:url.rfind("/")] + element.get("href")[5:])
    return link_dict


def write_to_file(link_dict, url):
    lang_dir = f"{data_dir}/{url[url.find('/') + 2:url.find('/') + 4]}"
    os.makedirs(lang_dir, exist_ok=True)
    with open(f"{lang_dir}/{windows_encode(urllib.parse.unquote(link_dict['name']))}.json", "w", encoding="utf-8") as file:
        file.write(json.dumps(link_dict, indent=4, ensure_ascii=False))


def run():
    page, url = get_page()
    soup = BeautifulSoup(page.text, "html.parser", parse_only=SoupStrainer(id="mw-content-text"))

    remove_unneeded_elements(soup)
    link_dict = extract_links(soup, url)
    write_to_file(link_dict, url)


if __name__ == "__main__":
    run()
