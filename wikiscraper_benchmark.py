import json
import sys
import time

import wikiscraper


def time_procedure():
    times = []
    for i in range(5):
        t1 = time.time()
        wikiscraper.run()
        t2 = time.time()
        times.append(t2-t1)
        time.sleep(3)
    return times, sum(times) / len(times)


if __name__ == "__main__":
    wikiscraper.data_dir = "./benchmark_results"

    benchmark_results = dict()
    sys.argv.append("https://de.wikipedia.org/wiki/Babice_u_Lesonic")
    benchmark_results[sys.argv[1]] = time_procedure()
    sys.argv[1] = "https://de.wikipedia.org/wiki/Deutsche_Nationalbibliothek"
    benchmark_results[sys.argv[1]] = time_procedure()
    sys.argv[1] = "https://en.wikipedia.org/wiki/Avengers:_Endgame"
    benchmark_results[sys.argv[1]] = time_procedure()
    print(benchmark_results)
    with open(f"{wikiscraper.data_dir}/results.json", "w", encoding="utf-8") as file:
        file.write(json.dumps(benchmark_results, indent=4, ensure_ascii=False))
